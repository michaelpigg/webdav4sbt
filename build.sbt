import de.johoop.jacoco4sbt._
import JacocoPlugin._

sbtPlugin := true

organization := "eu.diversit.sbt.plugin"

name := "webdav4sbt"

version := "1.3-sbt0.13"

publishMavenStyle := true

scalaVersion := "2.10.1"

libraryDependencies ++= Seq(
    "com.googlecode.sardine" % "sardine" % "146",
    "org.scalatest" % "scalatest_2.9.2" % "2.0.M5b" % "test",
    "com.typesafe"  % "config" % "1.0.0" % "test"
)

seq(jacoco.settings : _*)

